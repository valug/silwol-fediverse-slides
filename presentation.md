Das Fediverse
=============

Was ist es, wie funktioniert es und wie benutze ich es?
-------------------------------------------------------

Ein grober Überblick

2020-04-10

[Wolfgang Silbermayr](https://chaos.social/@silwol)

.footer: [VALUG.at](https://valug.at/) | [Source code](https://gitlab.com/valug/silwol-fediverse-slides) | [Lizenz: CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) | [Veranstaltungsseite](https://valug.at/Index/Vergangene%20Veranstaltungen/2020-04-10%20Das%20Fediverse)

---

Inhalt
======

- Allgemeines
- Screenshots
- Verschiedene Dienste
- Clients
- Das ActivityPub-Protokoll
- Instanzen
- Erfahrungen mit Microblogging
- Ideen / Ausblick
- Weitere Informationen

---

Allgemeines
===========

- Bin reiner Nutzer des Fediverse
- Beobachte interessiert
- Habe bislang nichts entwickelt
- Daher keine technischen Details

---

Screenshots (Tusky)
===================

![Tusky](images/tusky.png)

---

Screenshots (Tusky)
===================

![Tusky Beitrag erstellen](images/tusky-create.png)

---

Disclaimer
==========

- Aktuell sehr viel Entwicklungsaktivität
- Nutze und kenne nur wenig davon
- Daher nur unvollständiger Überblick möglich
- Möglicherweise verwaisen hier angeführte Projekte in Zukunft

---

Verschieden Dienste
===================

Bereits nutzbar
---------------

- Microblogging ([Mastodon](https://joinmastodon.org/), [Pleroma](https://pleroma.social/), …)
- Bilder-Sharing ([PixelFed](https://pixelfed.org/), …)
- Blogging ([WriteFreely](https://writefreely.org), [Plume](https://joinplu.me/), …)
- Video-Sharing ([PeerTube](https://joinpeertube.org/), …)
- Audio-Sharing, Podcasting ([Funkwhale](https://funkwhale.audio/), …)

Ausblick
--------

- Git-Hosting federation: Pull Requests etc. über Instanzen hinweg ([ForgeFed](https://forgefed.peers.community/))
- Link-Sharing und Diskussionen ([Lemmy](https://github.com/LemmyNet/lemmy))
- You name it…

---

Clients auf Android
===================

- [Tusky](https://tusky.app/) (auch auf [F-Droid](https://f-droid.org/packages/com.keylesspalace.tusky/))
- [FediLab](https://fedilab.app/) (auch auf [F-Droid](https://f-droid.org/packages/fr.gouv.etalab.mastodon/))
- [twitlatte](https://github.com/moko256/twitlatte) (auch auf [F-Droid](https://f-droid.org/de/packages/com.github.moko256.twitlatte/))
- [AndStatus](https://andstatus.org/) (auch auf [F-Droid](https://f-droid.org/de/packages/org.andstatus.app/))
- [NewPipe](https://newpipe.schabi.org/) für PeerTube (auch auf [F-Droid](https://f-droid.org/packages/org.schabi.newpipe/))

---

Das ActivityPub-Protokoll
=========================

- Baut auf Erfahrungen aus OStatus / pump.io
- [W3C Recommendation](https://www.w3.org/TR/2018/REC-activitypub-20180123/)
- Zwei Teile: server-to-server, client-to-server
- Nutzt JSON-LD, dadurch flexibel erweiterbar
- Inhalte liegen unverschlüsselt auf den Servern
- Bei Federation typischerweise Transportverschlüsselung (https)
  bei server-to-server und client-to-server.

---

Nutzung von Microblogging
=========================

- Nachrichten heißen "Toot"
- Interaktion mittels Antwort oder Boost (re-share in die eigene Timeline)
- Andere accounts können in die eigene Timeline abonniert werden ("Folgen")
- Content Warnings für sensible Themen und Bilder gewünscht
- Für mehrere "Identitäten" einfach mehrere Accounts nutzen (von den meisten Clients gut unterstützt)
- Meist Begrenzung auf 500 Zeichen, wobei Links immer fixe Länge haben
- Viele Instanzen invite-only

---

Nutzung von Microblogging
=========================

- Verwende meinen Account auf [chaos.social Mastodon-Instanz](https://chaos.social/) seit Oktober 2018
- [VALUG-Account](https://floss.social/@valug) seit Mai 2019 für Ankündigung der Treffen
- Generell willkommenes und freundliches Kommunikationsklima
- Bislang kaum Kontakte aus dem Real Life (ändert sich nach diesem Vortrag hoffentlich)
- Ich durchsuche einige Hashtags regelmäßig, stoße dadurch auf interessante User und Themen

---

Nutzung von Microblogging
=========================

- Einige Bots / Cross-Postings von Twitter (z.B. durch Multi-Account-Clients)
- Viele Crossposter dumpen Tweets, reagieren im Fediverse nicht
- Manche Instanzen blockieren andere komplett - Transparente Dokumentation hiervon hilfreich
- **Viele themenspezifische Instanzen**:
  - Motorsport: [mastd.racing](https://mastd.racing/),
  - Freie Software: [fosstodon.org](https://fosstodon.org), [floss.social](https://floss.social/)
  - Regionen: [bonn.social](https://bonn.social/) [toot.berlin](https://toot.berlin/)
  - Recht: [legal.social](https://legal.social/)
  - Kirchen: [kirche.social](https://kirche.social/),
  - Rechtextreme (hierfür keine Links)

---

Ideen / Ausblick
================

- Organisations-Instanzen: ([Debian](https://debian.social), [Grüne in DE](https://gruene.social), [Servus](https://social.servus.at/))
- Wünsche mehr Instanzen von Organisationen als offiziellen
  Kommunikationskanal.
- Clients für spezifische Anwendungsfälle (Audio, Video…)

---

Weitere Informationen
=====================

Text
----

- [netzpolitik.org Interview](https://netzpolitik.org/2017/interview-die-anfangsphase-des-alternativen-sozialen-netzwerks-mastodon-ist-vorueber/) mit [rixx](https://chaos.social/@rixx)

Audio / Video
-------------

- [Vortrag "Per Anhalter durch das Fediverse" (PrivacyWeek 2018)](https://media.ccc.de/v/pw18-80-per-anhalter-durch-das-fediverse-in-vier-teilen#t=533)
- [Chaosradio "Decentralize the Web"](https://chaosradio.de/cr249-decentralizetheweb)
- LibreLounge ActivityPub (englisch, mit ActivityPub Co-Autor [cwebber](https://octodon.social/@cwebber)): [Part 1](https://librelounge.org/episodes/episode-12-activitypub-part-1.html), [Part 2](https://librelounge.org/episodes/episode-17-activitypub-part-2.html), [Part 3](https://librelounge.org/episodes/episode-20-activitypub-part-3.html)
- [Rechtsbelehrung "Mastodon und Haftung für dezentrale Netzwerke"](https://rechtsbelehrung.com/mastodon-und-haftung-fuer-dezentrale-netzwerke-rechtsbelehrung-folge-60-jura-podcast/)
