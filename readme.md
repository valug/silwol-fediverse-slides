Präsentation zum Überblick über das Fediverse
=============================================

Die Präsentation wurde bei einiem [VALUG](https://valug.at/)-Treffen
gehalten.

Voraussetzung zum Bauen
-----------------------

- [Make](https://www.gnu.org/software/make/manual/make.html)
- [Darkslide](https://ionelmc.github.io/python-darkslide/)

Bauen
-----

- `make` im Hauptordner aufrufen
- Die Ausgabe wird unter `output/presentation.html` generiet
