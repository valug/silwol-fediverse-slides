OUTDIR=output
IMAGES=$(wildcard images/*)
OUTPUTS=$(OUTDIR)/presentation.html

all: $(OUTPUTS)

clean:
	rm -f $(OUTPUTS)

$(OUTDIR)/%.html: %.cfg %.md $(IMAGES)
	darkslide $< -i -d $@
	cp $< $(OUTDIR)/$<

.PHONY: all clean
